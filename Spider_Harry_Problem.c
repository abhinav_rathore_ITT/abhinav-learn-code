void operations(int noOfSpiders, int noOfSpiderDequeued, int powerOfSpiders[noOfSpiders], int position[noOfSpiders])
{
	int decrement[100],posOfDecreasedPower[100],positionOfMaxPower,index,decreasing,exactNoOfSpidersToDequeue,accessRemainingPowers,iterationToDecreasePower,iterationToFetchMaxPower,maximumPower,i,j;
	for(j=1;j<=noOfSpiderDequeued;j++)
    {
        maximumPower=-1;
        
        exactNoOfSpidersToDequeue = verifySpidersGreater(noOfSpiders,noOfSpiderDequeued);
        
        for(iterationToFetchMaxPower=1;iterationToFetchMaxPower<=exactNoOfSpidersToDequeue;iterationToFetchMaxPower++)
            if(powerOfSpiders[iterationToFetchMaxPower]>maximumPower)
            {
                maximumPower=powerOfSpiders[iterationToFetchMaxPower];
                positionOfMaxPower=position[iterationToFetchMaxPower];
            }
            
        decreasing=0;
        
        for(iterationToDecreasePower=1;iterationToDecreasePower<=noOfSpiderDequeued;iterationToDecreasePower++)
        {
            if(positionOfMaxPower==position[iterationToDecreasePower])
            {
                decreasing=1;
                continue;
            }
            
            if(powerOfSpiders[iterationToDecreasePower]>0)
                decrement[iterationToDecreasePower-decreasing]=powerOfSpiders[iterationToDecreasePower]-1;
            else
                decrement[iterationToDecreasePower-decreasing]=powerOfSpiders[iterationToDecreasePower];
            posOfDecreasedPower[iterationToDecreasePower-decreasing]=position[iterationToDecreasePower];
        }
        
        for(accessRemainingPowers=1;accessRemainingPowers<=noOfSpiders-noOfSpiderDequeued;accessRemainingPowers++)
        {
            powerOfSpiders[accessRemainingPowers]=powerOfSpiders[accessRemainingPowers+noOfSpiderDequeued];
            position[accessRemainingPowers]=position[accessRemainingPowers+noOfSpiderDequeued];
        }
        
        index=1;
        while(accessRemainingPowers<noOfSpiders)
        {
        	powerOfSpiders[accessRemainingPowers]=decrement[index];
            position[accessRemainingPowers]=posOfDecreasedPower[index++];
            accessRemainingPowers++;
		}
		
        noOfSpiders--;
        printf("%d ",positionOfMaxPower);
    }
    printf("\n");
    return 0;
}

int verifySpidersGreater(int noOfSpiders, int noOfSpiderDequeued)
{
	 	if(noOfSpiders<noOfSpiderDequeued)
           return noOfSpiders;
        else
           return noOfSpiderDequeued;
}

int main()
{ 
    int position[100],powerOfSpiders[100],noOfSpiders,noOfSpiderDequeued,numOfIterationsToGetPowerOfSpiders;
	scanf("%d%d",&noOfSpiders,&noOfSpiderDequeued);
	for(numOfIterationsToGetPowerOfSpiders=1;numOfIterationsToGetPowerOfSpiders<=noOfSpiders;numOfIterationsToGetPowerOfSpiders++)
	{
		scanf("%d",&powerOfSpiders[numOfIterationsToGetPowerOfSpiders]);
        position[numOfIterationsToGetPowerOfSpiders]=numOfIterationsToGetPowerOfSpiders;
	}
	operations(noOfSpiders,noOfSpiderDequeued,powerOfSpiders,position);
}