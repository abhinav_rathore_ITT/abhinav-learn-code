#include<iostream>
using namespace std;
int main()
{
	int array[3][3], sumOfDigitsAtOddPlace=0, sumOfDigitsAtEvenPlace=0;
	for(int i=0;i<3;i++)
	{
		for(int j=0;j<3;j++)
		{
			cin>>array[i][j];
			if((i+j)%2==0)
				sumOfDigitsAtOddPlace+=array[i][j];
			else
				sumOfDigitsAtEvenPlace+=array[i][j];
		}
	}
	cout<<sumOfDigitsAtOddPlace<<"\n"<<sumOfDigitsAtEvenPlace;
	return 0;
}
