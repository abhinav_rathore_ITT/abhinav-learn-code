#include<stdio.h>
#include<stdlib.h>
 
struct removeFriends
{	
    struct removeFriends *nextVal;
    int data;
};
 
struct removeFriends* creation(int value)
{
    struct removeFriends* newVal = (struct removeFriends*) malloc(sizeof(struct removeFriends));
    newVal->data = value;
    newVal->nextVal = NULL;
    return newVal;
}
 
struct removeFriends* deletion(struct removeFriends *top)
{
    if(top == NULL)
        return top;
    struct removeFriends *nextToTop = top->nextVal;
    free(top);
    return nextToTop;
}
 
struct removeFriends* insertion(struct removeFriends* top, struct removeFriends* newVal)
{
    if(top)
        newVal->nextVal = top;
    return newVal;
}
 
void display(struct removeFriends* headNode)
{
    if(headNode == NULL)
        return;
    display(headNode->nextVal);
    printf("%d ", headNode->data);
}
 
int main(){
    int testcases,noOfFriendsToDelete,noOfFriends,loopThroughNoOfFriends,remove,loopThroughNoOfTestcases,priority;
    struct removeFriends *topPlace = NULL, *newNode;
    scanf("%d", &testcases);
    for(loopThroughNoOfTestcases=0; loopThroughNoOfTestcases<testcases; loopThroughNoOfTestcases++)
	{
        scanf("%d %d", &noOfFriends, &noOfFriendsToDelete);
        remove = noOfFriendsToDelete;
        topPlace = NULL;
        for(loopThroughNoOfFriends=0; loopThroughNoOfFriends<noOfFriends; loopThroughNoOfFriends++)
		{
            scanf("%d", &priority);
            newNode = creation(priority);
            while(topPlace!=NULL && topPlace->data < priority && remove)
			{
                remove--;
                topPlace = deletion(topPlace);    
            }
            topPlace = insertion(topPlace, newNode);      
        }
        display(topPlace);
        printf("\n\n");
    }
    return 0;
}